﻿using System;
using System.Collections.Generic;
using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Domain.Interfaces;
using EmpresasDotNet.Domain.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EmpresasDotNet.Application.Controllers
{
    [Authorize("Bearer")]
    [ApiVersion("1")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService enterpriseService;

        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            this.enterpriseService = enterpriseService;
        }

        [HttpGet]
        [Route("api/v{version:apiVersion}/[controller]")]
        public IActionResult Get([FromQuery] EnterpriseEntity enterprise)
        {
            try
            {
                ICollection<EnterpriseEntity> enterprises = Request.QueryString.HasValue ? enterpriseService.Index(enterprise) : enterpriseService.Index();
                return Ok(new EnterprisesResponse()
                {
                    Enterprises = enterprises
                });
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("api/v{version:apiVersion}/[controller]/{id}")]
        public IActionResult GetUnique(int id)
        {
            try
            {
                EnterpriseEntity enterprise = enterpriseService.Show(id);
                return Ok(new EnterprisesResponse()
                {
                    Enterprise = enterprise
                });
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
