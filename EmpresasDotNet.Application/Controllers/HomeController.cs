﻿using Microsoft.AspNetCore.Mvc;

namespace EmpresasDotNet.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "It's working!";
        }

        [HttpPost]
        public string Post([FromBody] string value)
        {
            return value;
        }
    }
}
