﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using EmpresasDotNet.Application.Security;
using EmpresasDotNet.Domain.Contracts;
using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Domain.Interfaces;
using EmpresasDotNet.Domain.Responses;
using EmpresasDotNet.Infrastructure.CrossCutting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace EmpresasDotNet.Application.Controllers
{
    [AllowAnonymous]
    [ApiVersion("1")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IIvestdorService signInService;

        public UsersController(IIvestdorService signInService)
        {
            this.signInService = signInService;
        }

        [HttpPost]
        [Route("api/v{version:apiVersion}/[controller]/auth/sign_in")]
        public IActionResult SignIn(
            [FromBody] SignInContract user,
            [FromServices] SigningConfigurations signingConfigurations,
            [FromServices] TokenConfigurations tokenConfigurations
        )
        {
            try
            {
                InvestdorEntity investdor = signInService.SignIn(user);
                bool success = investdor != null;

                if (success)
                {
                    ClaimsIdentity identity = new ClaimsIdentity(
                        new GenericIdentity(investdor.Email, "uid"),
                        new[] {
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                            new Claim(JwtRegisteredClaimNames.UniqueName, investdor.Id.ToString())
                        }
                    );

                    DateTime dataCriacao = DateTime.Now;
                    DateTime dataExpiracao = dataCriacao + TimeSpan.FromSeconds(tokenConfigurations.Seconds);

                    JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                    SecurityToken securityToken = handler.CreateToken(new SecurityTokenDescriptor
                    {
                        Issuer = tokenConfigurations.Issuer,
                        Audience = tokenConfigurations.Audience,
                        SigningCredentials = signingConfigurations.SigningCredentials,
                        Subject = identity,
                        NotBefore = dataCriacao,
                        Expires = dataExpiracao
                    });

                    string token = handler.WriteToken(securityToken);

                    Response.Headers.Add("access-token", token);
                    Response.Headers.Add("token-type", "Bearer");
                    Response.Headers.Add("client", Hash.ComputeSha256(investdor.Id.ToString()));
                    Response.Headers.Add("uid", investdor.Email);
                }

                return Ok(new SignInResponse()
                {
                    Success = success,
                    Errors = !success ? new List<string>() { "Invalid login credentials. Please try again." } : new List<string>(),
                    Investdor = investdor
                });
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
