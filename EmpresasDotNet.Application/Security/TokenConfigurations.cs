﻿namespace EmpresasDotNet.Application.Security
{
    public class TokenConfigurations
    {
        public string Audience => "EmpresasDotNet";

        public string Issuer => "EmpresasDotNet";

        public int Seconds => 3600;
    }
}
