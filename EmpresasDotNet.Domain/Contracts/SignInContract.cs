﻿using EmpresasDotNet.Infrastructure.CrossCutting;

namespace EmpresasDotNet.Domain.Contracts
{
    public class SignInContract
    {
        public string Email { get; set; }
        
        public string Password { get => password; set => password = Hash.ComputeSha256(value); }

        private string password;
    }
}
