﻿using Newtonsoft.Json;

namespace EmpresasDotNet.Domain.Entities
{
    public abstract class BaseEntity
    {
        [JsonProperty(Order = 1)]
        public virtual int Id { get; set; }
    }
}
