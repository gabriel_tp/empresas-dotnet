﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmpresasDotNet.Domain.Entities
{
    public class EnterpriseEntity : BaseEntity
    {
        public EnterpriseEntity()
        {
            EnterpriseType = new EnterpriseTypeEntity();
            PortfolioEnterprises = new List<PortfolioEnterpriseEntity>();
        }

        [JsonProperty(Order = 2)]
        public virtual string Email { get; set; }

        [JsonProperty(Order = 3)]
        public virtual string Facebook { get; set; }

        [JsonProperty(Order = 4)]
        public virtual string Twitter { get; set; }

        [JsonProperty(Order = 5)]
        public virtual string LinkedIn { get; set; }

        [JsonProperty(Order = 6)]
        public virtual string Phone { get; set; }

        [JsonProperty(Order = 7)]
        public virtual bool? OwnEnterprise { get; set; }

        [JsonProperty(Order = 8)]
        public virtual string Name { get; set; }

        [JsonProperty(Order = 9)]
        public virtual string Photo { get; set; }

        [JsonProperty(Order = 10)]
        public virtual string Description { get; set; }

        [JsonProperty(Order = 11)]
        public virtual string City { get; set; }

        [JsonProperty(Order = 12)]
        public virtual string Country { get; set; }

        [JsonProperty(Order = 13)]
        public virtual double? Value { get; set; }

        [JsonProperty(Order = 14)]
        public virtual double? SharePrice { get; set; }

        [JsonProperty(Order = 15)]
        public virtual EnterpriseTypeEntity EnterpriseType { get; set; }

        [NotMapped]
        [JsonIgnore]
        public int? Enterprise_Types { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<PortfolioEnterpriseEntity> PortfolioEnterprises { get; set; }
    }
}
