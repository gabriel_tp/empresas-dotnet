﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmpresasDotNet.Domain.Entities
{
    public class EnterpriseTypeEntity : BaseEntity
    {
        public EnterpriseTypeEntity()
        {
            Enterprises = new List<EnterpriseEntity>();
        }

        [JsonProperty(Order = 2)]
        public virtual string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<EnterpriseEntity> Enterprises { get; set; }
    }
}
