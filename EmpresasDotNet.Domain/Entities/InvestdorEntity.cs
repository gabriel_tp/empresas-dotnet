﻿using EmpresasDotNet.Infrastructure.CrossCutting;
using Newtonsoft.Json;

namespace EmpresasDotNet.Domain.Entities
{
    public class InvestdorEntity : BaseEntity
    {
        public InvestdorEntity()
        {
            Portfolio = new PortfolioEntity();
        }

        [JsonProperty(Order = 2)]
        public virtual string Name { get; set; }

        [JsonProperty(Order = 3)]
        public virtual string Email { get; set; }

        [JsonProperty(Order = 4)]
        public virtual string City { get; set; }

        [JsonProperty(Order = 5)]
        public virtual string Country { get; set; }

        [JsonProperty(Order = 6)]
        public virtual double Balance { get; set; }

        [JsonProperty(Order = 7)]
        public virtual string Photo { get; set; }

        [JsonProperty(Order = 8)]
        public virtual PortfolioEntity Portfolio { get; set; }

        [JsonProperty(Order = 9)]
        public virtual double PortfolioValue { get; set; }

        [JsonProperty(Order = 10)]
        public virtual bool FirstAccess { get; set; }

        [JsonProperty(Order = 11)]
        public virtual bool SuperAngel { get; set; }

        [JsonIgnore]
        public virtual string Password { get => password; set => password = Hash.ComputeSha256(value); }

        [JsonIgnore]
        private string password;
    }
}
