﻿namespace EmpresasDotNet.Domain.Entities
{
    public class PortfolioEnterpriseEntity
    {
        public virtual int PortfolioId { get; set; }

        public virtual PortfolioEntity Portfolio { get; set; }

        public virtual int EnterpriseId { get; set; }

        public virtual EnterpriseEntity Enterprise { get; set; }
    }
}
