﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmpresasDotNet.Domain.Entities
{
    public class PortfolioEntity : BaseEntity
    {
        public PortfolioEntity()
        {
            Investdors = new List<InvestdorEntity>();
            Enterprises = new List<PortfolioEnterpriseEntity>();
        }

        [JsonIgnore]
        public override int Id { get => base.Id; set => base.Id = value; }

        [JsonIgnore]
        public virtual ICollection<InvestdorEntity> Investdors { get; set; }

        [JsonProperty(Order = 2)]
        public virtual ICollection<PortfolioEnterpriseEntity> Enterprises { get; set; }

        [NotMapped]
        [JsonProperty(Order = 1)]
        public int Enterprises_number => Enterprises.Count;
    }
}
