﻿using EmpresasDotNet.Domain.Entities;
using System.Collections.Generic;

namespace EmpresasDotNet.Domain.Interfaces
{
    public interface IEnterpriseRepository : IRepository<EnterpriseEntity>
    {
        ICollection<EnterpriseEntity> Index();

        ICollection<EnterpriseEntity> Index(EnterpriseEntity filter);

        EnterpriseEntity Show(int id);
    }
}
