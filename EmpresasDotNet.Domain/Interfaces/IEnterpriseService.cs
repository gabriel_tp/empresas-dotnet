﻿using EmpresasDotNet.Domain.Entities;
using System.Collections.Generic;

namespace EmpresasDotNet.Domain.Interfaces
{
    public interface IEnterpriseService : IService<EnterpriseEntity>
    {
        ICollection<EnterpriseEntity> Index();

        ICollection<EnterpriseEntity> Index(EnterpriseEntity filter);

        EnterpriseEntity Show(int id);
    }
}
