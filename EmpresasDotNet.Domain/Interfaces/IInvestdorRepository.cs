﻿using EmpresasDotNet.Domain.Contracts;
using EmpresasDotNet.Domain.Entities;

namespace EmpresasDotNet.Domain.Interfaces
{
    public interface IInvestdorRepository : IRepository<InvestdorEntity>
    {
        InvestdorEntity SignIn(SignInContract user);
    }
}
