﻿using EmpresasDotNet.Domain.Contracts;
using EmpresasDotNet.Domain.Entities;

namespace EmpresasDotNet.Domain.Interfaces
{
    public interface IIvestdorService : IService<InvestdorEntity>
    {
        InvestdorEntity SignIn(SignInContract user);
    }
}
