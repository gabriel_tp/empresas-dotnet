﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace EmpresasDotNet.Domain.Responses
{
    public class BaseResponse
    {
        public BaseResponse()
        {
            Errors = new List<string>();
        }

        [JsonProperty(Order = 998)]
        public virtual bool Success { get; set; }

        [JsonProperty(Order = 999)]
        public virtual ICollection<string> Errors {get;set;}

        public bool ShouldSerializeErrors()
        {
            return !Success && Errors.Any();
        }
    }
}
