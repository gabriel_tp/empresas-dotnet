﻿using EmpresasDotNet.Domain.Entities;
using System.Collections.Generic;

namespace EmpresasDotNet.Domain.Responses
{
    public class EnterprisesResponse : BaseResponse
    {
        public ICollection<EnterpriseEntity> Enterprises { get; set; }

        public EnterpriseEntity Enterprise { get; set; }

        public override bool Success { get; set; }

        public bool ShouldSerializeEnterprises()
        {
            return Enterprises != null;
        }

        public bool ShouldSerializeEnterprise()
        {
            return Enterprise != null;
        }

        public bool ShouldSerializeSuccess()
        {
            return false;
        }
    }
}
