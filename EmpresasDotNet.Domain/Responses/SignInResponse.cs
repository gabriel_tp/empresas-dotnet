﻿using EmpresasDotNet.Domain.Entities;
using Newtonsoft.Json;

namespace EmpresasDotNet.Domain.Responses
{
    public class SignInResponse : BaseResponse
    {
        [JsonProperty(Order = 1)]
        public InvestdorEntity Investdor { get; set; }

        public bool ShouldSerializeInvestdor()
        {
            return Investdor != null;
        }
    }
}
