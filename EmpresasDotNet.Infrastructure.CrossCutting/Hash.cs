﻿using System.Security.Cryptography;
using System.Text;

namespace EmpresasDotNet.Infrastructure.CrossCutting
{
    public static class Hash
    {
        public static string ComputeSha256(string rawData)
        {
            StringBuilder builder = new StringBuilder();

            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }
    }
}
