﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace EmpresasDotNet.Infrastructure.Data.Context
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<EmpresasDotNetContext>
    {
        public EmpresasDotNetContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<EmpresasDotNetContext> builder = new DbContextOptionsBuilder<EmpresasDotNetContext>();
            builder.UseSqlServer(EmpresasDotNetContext.CONNECTIONSTRING);
            return new EmpresasDotNetContext(builder.Options);
        }
    }
}
