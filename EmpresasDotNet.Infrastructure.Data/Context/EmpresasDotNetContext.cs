﻿using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Infrastructure.Data.Mapping;
using Microsoft.EntityFrameworkCore;

namespace EmpresasDotNet.Infrastructure.Data.Context
{
    public class EmpresasDotNetContext : DbContext
    {
        public static string CONNECTIONSTRING => @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Projetos\empresas-dotnet\EmpresasDotNet.Infrastructure.Data\EmpresasDotNet.mdf;Integrated Security=True;Connect Timeout=30";

        public DbSet<EnterpriseEntity> Enterprise { get; set; }

        public DbSet<EnterpriseTypeEntity> EnterpriseType { get; set; }

        public DbSet<InvestdorEntity> Investdor { get; set; }

        public DbSet<PortfolioEntity> Portfolio { get; set; }

        public DbSet<PortfolioEnterpriseEntity> PortfolioEnterprises { get; set; }

        public EmpresasDotNetContext(DbContextOptions<EmpresasDotNetContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(CONNECTIONSTRING);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EnterpriseEntity>(new EnterpriseMap().Configure);
            modelBuilder.Entity<EnterpriseTypeEntity>(new EnterpriseTypeMap().Configure);
            modelBuilder.Entity<InvestdorEntity>(new InvestdorMap().Configure);
            modelBuilder.Entity<PortfolioEntity>(new PortfolioMap().Configure);
            modelBuilder.Entity<PortfolioEnterpriseEntity>(new PortfolioEnterpriseMap().Configure);
        }
    }
}
