﻿using EmpresasDotNet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmpresasDotNet.Infrastructure.Data.Mapping
{
    public class EnterpriseMap : IEntityTypeConfiguration<EnterpriseEntity>
    {
        public void Configure(EntityTypeBuilder<EnterpriseEntity> builder)
        {
            builder.ToTable("Enterprise");

            builder.HasKey(x => x.Id);

            builder.Property(c => c.Email).HasColumnName("Email");
            builder.Property(c => c.Facebook).HasColumnName("Facebook");
            builder.Property(c => c.Twitter).HasColumnName("Twitter");
            builder.Property(c => c.LinkedIn).HasColumnName("LinkedIn");
            builder.Property(c => c.Phone).HasColumnName("Phone");
            builder.Property(c => c.OwnEnterprise).HasColumnName("OwnEnterprise");
            builder.Property(c => c.Name).HasColumnName("Name");
            builder.Property(c => c.Photo).HasColumnName("Photo");
            builder.Property(c => c.Description).HasColumnName("Description");
            builder.Property(c => c.City).HasColumnName("City");
            builder.Property(c => c.Country).HasColumnName("Country");
            builder.Property(c => c.Value).HasColumnName("Value");
            builder.Property(c => c.SharePrice).HasColumnName("SharePrice");

            builder.HasOne(x => x.EnterpriseType).WithMany(x => x.Enterprises);
        }
    }
}
