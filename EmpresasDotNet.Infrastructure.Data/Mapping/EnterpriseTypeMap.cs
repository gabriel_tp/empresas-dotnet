﻿using EmpresasDotNet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmpresasDotNet.Infrastructure.Data.Mapping
{
    public class EnterpriseTypeMap : IEntityTypeConfiguration<EnterpriseTypeEntity>
    {
        public void Configure(EntityTypeBuilder<EnterpriseTypeEntity> builder)
        {
            builder.ToTable("EnterpriseType");

            builder.HasKey(x => x.Id);

            builder.Property(c => c.Name).HasColumnName("Name");

            builder.HasMany(x => x.Enterprises).WithOne(x => x.EnterpriseType);
        }
    }
}
