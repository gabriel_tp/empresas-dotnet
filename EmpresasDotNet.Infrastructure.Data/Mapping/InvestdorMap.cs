﻿using EmpresasDotNet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmpresasDotNet.Infrastructure.Data.Mapping
{
    public class InvestdorMap : IEntityTypeConfiguration<InvestdorEntity>
    {
        public void Configure(EntityTypeBuilder<InvestdorEntity> builder)
        {
            builder.ToTable("Investdor");

            builder.HasKey(x => x.Id);

            builder.Property(c => c.Name).HasColumnName("Name");
            builder.Property(c => c.Email).HasColumnName("Email");
            builder.Property(c => c.City).HasColumnName("City");
            builder.Property(c => c.Country).HasColumnName("Country");
            builder.Property(c => c.Balance).HasColumnName("Balance");
            builder.Property(c => c.Photo).HasColumnName("Photo");
            builder.Property(c => c.PortfolioValue).HasColumnName("PortfolioValue");
            builder.Property(c => c.FirstAccess).HasColumnName("FirstAccess");
            builder.Property(c => c.SuperAngel).HasColumnName("SuperAngel");
            builder.Property(c => c.Password).HasColumnName("Password");

            builder.HasOne(x => x.Portfolio).WithMany(x => x.Investdors);
        }
    }
}
