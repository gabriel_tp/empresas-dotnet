﻿using EmpresasDotNet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmpresasDotNet.Infrastructure.Data.Mapping
{
    public class PortfolioEnterpriseMap : IEntityTypeConfiguration<PortfolioEnterpriseEntity>
    {
        public void Configure(EntityTypeBuilder<PortfolioEnterpriseEntity> builder)
        {
            builder.ToTable("PortfolioEnterprise");

            builder.HasKey(x => new { x.PortfolioId, x.EnterpriseId });

            builder.HasOne(x => x.Portfolio).WithMany(x => x.Enterprises).HasForeignKey(x => x.PortfolioId);
            builder.HasOne(x => x.Enterprise).WithMany(x => x.PortfolioEnterprises).HasForeignKey(x => x.EnterpriseId);
        }
    }
}
