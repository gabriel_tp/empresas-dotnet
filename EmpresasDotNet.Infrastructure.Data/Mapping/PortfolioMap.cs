﻿using EmpresasDotNet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmpresasDotNet.Infrastructure.Data.Mapping
{
    public class PortfolioMap : IEntityTypeConfiguration<PortfolioEntity>
    {
        public void Configure(EntityTypeBuilder<PortfolioEntity> builder)
        {
            builder.ToTable("Portfolio");

            builder.HasKey(x => x.Id);

            builder.HasMany(x => x.Investdors).WithOne(x => x.Portfolio);
        }
    }
}
