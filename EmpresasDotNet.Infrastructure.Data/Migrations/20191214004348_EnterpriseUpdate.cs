﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmpresasDotNet.Infrastructure.Data.Migrations
{
    public partial class EnterpriseUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Enterprise",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Enterprise");
        }
    }
}
