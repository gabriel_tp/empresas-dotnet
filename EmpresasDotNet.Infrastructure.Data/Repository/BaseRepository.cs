﻿using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Domain.Interfaces;
using EmpresasDotNet.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EmpresasDotNet.Infrastructure.Data.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly EmpresasDotNetContext context;

        public BaseRepository()
        {
            DbContextOptionsBuilder<EmpresasDotNetContext> builder = new DbContextOptionsBuilder<EmpresasDotNetContext>();
            builder.UseSqlServer(EmpresasDotNetContext.CONNECTIONSTRING);
            context = new EmpresasDotNetContext(builder.Options);
        }

        public void Insert(T obj)
        {
            context.Set<T>().Add(obj);
            context.SaveChanges();
        }

        public void Update(T obj)
        {
            context.Entry(obj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<T>().Remove(Select(id));
            context.SaveChanges();
        }

        public IList<T> Select()
        {
            return context.Set<T>().ToList();
        }

        public T Select(int id)
        {
            return context.Set<T>().Find(id);
        }
    }
}
