﻿using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EmpresasDotNet.Infrastructure.Data.Repository
{
    public class EnterpriseRepository : BaseRepository<EnterpriseEntity>, IEnterpriseRepository
    {
        public ICollection<EnterpriseEntity> Index()
        {
            return context.Enterprise.Include(x => x.EnterpriseType).ToList();
        }

        public ICollection<EnterpriseEntity> Index(EnterpriseEntity filter)
        {
            return context.Enterprise.Include(x => x.EnterpriseType).Where(x =>
                (string.IsNullOrWhiteSpace(filter.City) || x.City.Contains(filter.City)) &&
                (string.IsNullOrWhiteSpace(filter.Country) || x.Country.Contains(filter.Country)) &&
                (string.IsNullOrWhiteSpace(filter.Description) || x.Description.Contains(filter.Description)) &&
                (string.IsNullOrWhiteSpace(filter.Email) || x.Email.Contains(filter.Email)) &&
                (filter.EnterpriseType.Id.Equals(0) || x.EnterpriseType.Id.Equals(filter.EnterpriseType.Id)) &&
                (filter.Enterprise_Types == null || x.EnterpriseType.Id == filter.Enterprise_Types) &&
                (string.IsNullOrWhiteSpace(filter.Facebook) || x.Facebook.Contains(filter.Facebook)) &&
                (string.IsNullOrWhiteSpace(filter.LinkedIn) || x.LinkedIn.Contains(filter.LinkedIn)) &&
                (string.IsNullOrWhiteSpace(filter.Name) || x.Name.Contains(filter.Name)) &&
                (filter.OwnEnterprise == null || x.OwnEnterprise == filter.OwnEnterprise) &&
                (string.IsNullOrWhiteSpace(filter.Phone) || x.Phone.Contains(filter.Phone)) &&
                (string.IsNullOrWhiteSpace(filter.Photo) || x.Photo.Contains(filter.Photo)) &&
                (filter.SharePrice == null || x.SharePrice.Equals(filter.SharePrice)) &&
                (string.IsNullOrWhiteSpace(filter.Twitter) || x.Twitter.Contains(filter.Twitter)) &&
                (filter.Value == null || x.Value.Equals(filter.Value))
            ).ToList();
        }

        public EnterpriseEntity Show(int id)
        {
            return context.Enterprise.Include(x => x.EnterpriseType).FirstOrDefault(x => x.Id == id);
        }
    }
}
