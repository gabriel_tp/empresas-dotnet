﻿using EmpresasDotNet.Domain.Contracts;
using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EmpresasDotNet.Infrastructure.Data.Repository
{
    public class InvestdorRepository : BaseRepository<InvestdorEntity>, IInvestdorRepository
    {
        public InvestdorEntity SignIn(SignInContract user)
        {
            return context.Investdor.Include(x => x.Portfolio).FirstOrDefault(x => x.Email == user.Email && x.Password == user.Password);
        }
    }
}
