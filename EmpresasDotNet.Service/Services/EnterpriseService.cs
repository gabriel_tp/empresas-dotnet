﻿using System.Collections.Generic;
using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Domain.Interfaces;

namespace EmpresasDotNet.Service.Services
{
    public class EnterpriseService : BaseService<EnterpriseEntity>, IEnterpriseService
    {
        private readonly IEnterpriseRepository enterpriseRepository;

        public EnterpriseService(IEnterpriseRepository repository) : base(repository)
        {
            enterpriseRepository = repository;
        }

        public ICollection<EnterpriseEntity> Index()
        {
            return enterpriseRepository.Index();
        }

        public ICollection<EnterpriseEntity> Index(EnterpriseEntity filter)
        {
            return enterpriseRepository.Index(filter);
        }

        public EnterpriseEntity Show(int id)
        {
            return enterpriseRepository.Show(id);
        }
    }
}
