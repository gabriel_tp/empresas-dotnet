﻿using EmpresasDotNet.Domain.Contracts;
using EmpresasDotNet.Domain.Entities;
using EmpresasDotNet.Domain.Interfaces;

namespace EmpresasDotNet.Service.Services
{
    public class InvestdorService : BaseService<InvestdorEntity>, IIvestdorService
    {
        private readonly IInvestdorRepository investdorRepository;

        public InvestdorService(IInvestdorRepository repository) : base(repository)
        {
            investdorRepository = repository;
        }

        public InvestdorEntity SignIn(SignInContract user)
        {
            return investdorRepository.SignIn(user);
        }
    }
}
